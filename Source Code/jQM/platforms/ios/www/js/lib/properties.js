define(function () {
    var propModule = function () {
        var ballValues = [];
        var constructor,
            that = {},
            my = {},
            buttonColour,
            type,
            number,
            ballsize,
            ballXPosition,
            ballYPosition,
            accelRate,
            buttonName,
            snapQuali;

        // a = number, b = type
        constructor = function (a, b) {
            number = a;
            type = b;
            return that;
        };
        //[size, XPos, Ypos]
        that.setBallValues = function (a, b, c) {
            ballValues.push(a);
            ballValues.push(b);
            ballValues.push(c);
        }

        that.getBallValues = function () {
            return ballValues;
        }

        that.getType = function () {
            return type;
        }

        that.getNumber = function () {
            return number;
        }

        that.getUniqueName = function () {
            return type + number;
        }

        that.setAccelRate = function (ar) {
            accelRate = ar;
        }

        that.getAccelRate = function () {
            return accelRate;
        }

        that.setButtonName = function (bN) {
            buttonName = bN;
        }

        that.getButtonName = function () {
            return buttonName;
        }

        that.setButtonColour = function (col) {
            buttonColour = col;
        }

        that.getButtonColour = function () {
            return buttonColour;
        }

        that.getNumber = function () {
            return number;
        }

        that.setSnapQuali = function (sQ) {
            snapQuali = sQ;
        }

        that.getSnapQuali = function () {
            return snapQuali;
        }

        return constructor.apply(null, arguments);
    };

    return propModule;

});