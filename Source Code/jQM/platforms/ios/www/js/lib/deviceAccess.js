define(['ballDrawer'], function (ballDrawer) {
    var watchID = null,
        xAcc,
        yAcc,
        samplingRate,
        xTouch,
        yTouch,
        snapQuali,
        state = null,
        imageSource,
        picTaken,
        imgSrc;

    startStopAccelerometerWatch = function (kind, ms) {
        var milli = parseInt(ms);
        if (kind.trim() == "start" && state != "start") {
            var options = {
                frequency: milli
            };
            state = kind.trim();
            watchID = navigator.accelerometer.watchAcceleration(onSuccessAccelerometer, onErrorAccelerometer, options);
        } else if (kind.trim() == "stop") {
            if (watchID) {
                navigator.accelerometer.clearWatch(watchID);
                state = "stop";
                watchID = null;
            }
        }
    }

    getSnap = function () {
        picTaken = $.Deferred();
        var deferred = $.Deferred();
        navigator.camera.getPicture(onSuccessCam, onErrorCam, {
            quality: getSnapQuali(),
            destinationType: Camera.DestinationType.DATA_URL
        });
        $.when(picTaken).done(function () {
            deferred.resolveWith(this, [imageSource]);
        });
        return deferred;
    }

    drawSvg = function (uN) {
        var pageWidth = $("#programPage").width(),
            pageHeight = $("#programPage").height();
        var paper = Raphael("programPage", pageWidth, pageHeight);
        paper.canvas.setAttribute('id', uN);
        ballDrawer.setPaper(paper);
    }

    onSuccessCam = function (imageData) {
        imageSource = "data:image/jpeg;base64," + imageData;
        picTaken.resolve();
    }

    onSuccessAccelerometer = function (acceleration) {
        xAcc = acceleration.x;
        yAcc = acceleration.y;
        ///ballDrawer.redrawBall(acceleration);
    }

    onErrorCam = function (e) {
        alert("Failed because: " + e);
    }

    onErrorAccelerometer = function (e) {
        alert("Failed because: " + e);
    }

    getXAcceleration = function () {
        return xAcc;
    };

    getYAcceleration = function () {
        return yAcc;
    };

    setSamplingRate = function (sR) {
        samplingRate = sR;
    };

    getSamplingRate = function () {
        return samplingRate;
    };

    printXTouch = function (x) {
        $("#programPage").on("touchstart", function (e) {
            e.preventDefault();
            alert(e.originalEvent.touches[0].pageX);
        });
    };

    printYTouch = function (y) {
        $("#programPage").on("touchstart", function (e) {
            e.preventDefault();
            alert(e.originalEvent.touches[0].pageY);
        });
    };

    getXTouch = function () {
        return xTouch;
    };

    getYTouch = function () {
        return yTouch;
    };

    setSnapQuali = function (sQ) {
        snapQuali = sQ;
    };

    getSnapQuali = function () {
        return snapQuali;
    };

    setImgSource = function (imgS) {
        imgSrc = imgS;
    };

    getImgSource = function () {
        return imgSrc;
    };

    return {
        startStopAccelerometerWatch: startStopAccelerometerWatch,
        getXAcceleration: getXAcceleration,
        getYAcceleration: getYAcceleration,
        setSamplingRate: setSamplingRate,
        getSamplingRate: getSamplingRate,
        printXTouch: printXTouch,
        printYTouch: printYTouch,
        getXTouch: getXTouch,
        getYTouch: getYTouch,
        setSnapQuali: setSnapQuali,
        getSnapQuali: getSnapQuali,
        getSnap: getSnap,
        setImgSource: setImgSource,
        getImgSource: getImgSource,
        drawSvg: drawSvg
    }
});