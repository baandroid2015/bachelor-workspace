'use strict';

goog.provide('Blockly.JavaScript.accelerometer');

goog.require('Blockly.JavaScript');

Blockly.JavaScript['accel_xvalue'] = function (block) {
    var dropdown_name = block.getFieldValue('NAME');
    var code = 'deviceAccess.getXAcceleration()';
    return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.JavaScript['accel_yvalue'] = function (block) {
    var dropdown_name = block.getFieldValue('NAME');
    var code = 'deviceAccess.getYAcceleration()';
    return [code, Blockly.JavaScript.ORDER_NONE];
};