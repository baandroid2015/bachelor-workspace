'use strict';

goog.provide('Blockly.JavaScript.button');

goog.require('Blockly.JavaScript');

Blockly.JavaScript['button_click'] = function (block) {
    var dropdown_name = block.getFieldValue('NAME');
    var statements_statement = Blockly.JavaScript.statementToCode(block, 'statement');
    var code = '$("#' + dropdown_name + '-Program").on("tap", function() {' +  statements_statement + '});';
    return code;
};