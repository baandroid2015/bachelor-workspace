// return constructor.apply(null, arguments); Erklärung:
// rufe den constructor im Kontext von null auf übergebe ihm die arguments
// Bsp.: var objekt = {eigenschaft = 0}; 
// function summe (a,b,c) {this.summe = a+b+c; alert(this.summe);}
// summe.apply(objekt, [1,2,3]);
// mit call summe.apply(objekt, 1, 2, 3,);
define(['properties', 'collection/handlebars', 'ballDrawer', 'deviceAccess'], function (props, handlebars, ballDrawer, deviceAcccess) {
    var itemModule = function () {
        var construnctor,
            that = {},
            my = {},
            properties;
        // a = number, b = type
        constructor = function (a, b) {
            properties = new props(a, b);
            return that;
        };

        that.setProperties = function () {
            if (properties.getType().trim() == "Ball") {
                properties.setBallValues($("#ballSize").val(), $("#xpos").val(), $("#ypos").val());
            } else if (properties.getType().trim() == "Accelerometer") {
                properties.setAccelRate($("#accelRate").val().trim());
                deviceAcccess.setSamplingRate($("#accelRate").val().trim());
            } else if (properties.getType().trim() == "Button") {
                properties.setButtonColour($("#colourfieldset :radio:checked").val());
            } else if (properties.getType().trim() == "Camera") {
                properties.setSnapQuali($("#snapQuali").val().trim());
                deviceAcccess.setSnapQuali($("#snapQuali").val().trim());
            }
        };

        that.drawItem = function () {
            //http://localhost/Heiko/progTemplate.html
            //tpl/template.html
            //draw Items for first Page
            var tpl = handlebars.compile(my.ajaxCall("http://localhost/Heiko/template.html"));
            $("#pageOneDiv").append(tpl({
                divID: properties.getUniqueName(),
                headerID: properties.getUniqueName() + "-Header",
                itemName: properties.getUniqueName()
            })).collapsibleset('refresh');

            //draw Button
            if (properties.getType().trim() == "Button") {
                var tpl = handlebars.compile(my.ajaxCall("http://localhost/Heiko/progTemplate.html"));
                $("#controlBtns").append(tpl({
                    itemProgID: properties.getUniqueName() + "-Program",
                    itemProgName: properties.getUniqueName()
                }));
                $("#" + properties.getUniqueName() + "-Program").css("background-color", properties.getButtonColour());
                if (properties.getNumber() > 1) {
                    $("#" + properties.getUniqueName() + "-Program").css("left", properties.getNumber() * 10 - 10 + "%");
                }
                //$("#itemsPageOne").listview().listview('refresh');
            }

            // draw Ball
            else if (properties.getType().trim() == "Ball") {
                ballDrawer.drawBall(properties.getUniqueName().trim(), properties.getBallValues());
            }

            ///draw SVG
            else if (properties.getType().trim() == "ScalableVectorGraphics") {
                deviceAcccess.drawSvg(properties.getUniqueName().trim());
            }
        };

        that.getProperties = function () {
            return properties;
        };

        my.ajaxCall = function (path) {
            var template;
            $.ajax({
                url: path,
                async: false,
                cache: true,
                success: function (data) {
                    template = data;
                }
            });
            return template;
        };
        return constructor.apply(null, arguments);
    };
    return itemModule;
});