'use strict';

goog.provide('Blockly.Blocks.accelerometer');

goog.require('Blockly.Blocks');

Blockly.Blocks['accel_xvalue'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("get 'x' value of")
            .appendField(new Blockly.FieldDropdown(createAccelerometerDropdown), "NAME");
        this.setInputsInline(false);
        this.setOutput(true, 'xAccel');
        this.setColour(315);
        this.setTooltip('');
        this.setHelpUrl('http://www.example.com/');
    }
};

Blockly.Blocks['accel_yvalue'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("get 'y' value of")
            .appendField(new Blockly.FieldDropdown(createAccelerometerDropdown), "NAME");
        this.setInputsInline(false);
        this.setOutput(true, 'yAccel');
        this.setColour(315);
        this.setTooltip('');
        this.setHelpUrl('http://www.example.com/');
    }
};



function createAccelerometerDropdown() {
    var dropdownOptions = [];
    if (itemArray.length != 0) {
        for (var i = 0; i < itemArray.length; i++) {
            //dropdownOptions.push([itemArray[i].getProperties().getName(), itemArray[i].getProperties().getGuid()]);
            if (itemArray[i].getProperties().getType().trim() == "Accelerometer") {
                dropdownOptions.push([itemArray[i].getProperties().getUniqueName(), itemArray[i].getProperties().getUniqueName()]);
            }
        }
        if (dropdownOptions.length != 0) {
            return dropdownOptions;
        } else {
            return "-";
        }
    } else {
        return "-";
    }
}