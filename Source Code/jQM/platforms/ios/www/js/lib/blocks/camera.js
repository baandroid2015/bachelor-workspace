'use strict';

goog.provide('Blockly.Blocks.camera');

goog.require('Blockly.Blocks');

Blockly.Blocks['cam_getsnap'] = {
    init: function () {
        this.appendDummyInput()
            .appendField(new Blockly.FieldDropdown(createCamDropdown), "NAME")
            .appendField(".getSnapshot");
        this.setOutput(true);
        this.setColour(180);
        this.setTooltip('');
        this.setHelpUrl('http://www.example.com/');
    }
};

Blockly.Blocks['cam_setrear'] = {
    init: function () {
        this.appendValueInput("NAME")
            .appendField(".setAppBackground")
            .setCheck("Colour");
        this.setInputsInline(true);
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setColour(180);
        this.setTooltip('');
        this.setHelpUrl('http://www.example.com/');
    }
};


function createCamDropdown() {
    var dropdownOptions = [];
    if (itemArray.length != 0) {
        for (var i = 0; i < itemArray.length; i++) {
            //dropdownOptions.push([itemArray[i].getProperties().getName(), itemArray[i].getProperties().getGuid()]);
            if (itemArray[i].getProperties().getType().trim() == "Camera") {
                dropdownOptions.push([itemArray[i].getProperties().getUniqueName(), itemArray[i].getProperties().getUniqueName()]);
            }
        }
        if (dropdownOptions.length != 0) {
            return dropdownOptions;
        } else {
            return "-";
        }
    } else {
        return "-";
    }
}