define(['ballDrawer'], function (ballDrawer) {
    var workspace;

    initBl = function () {
        workspace = Blockly.inject('blocklyDiv', {
            toolbox: document.getElementById('toolbox'),
            grid: {
                spacing: 20,
                length: 3,
                colour: '#00ff00',
                snap: true
            },
            trashcan: true,
            scrollbars: false
        });
        workspace.addChangeListener(removeDoubleBlocks);
    };

    removeDoubleBlocks = function () {
        var allCustomWorkspaceBlocks = [];
        var customAccessBlocks = [];
        var counts = {};
        var allBlocks = Blockly.mainWorkspace.getAllBlocks();

        //write all Blocks to array
        for (var i = 0; i < allBlocks.length; i++) {
            if (allBlocks[i].getFieldValue("NAME") != null) {
                allCustomWorkspaceBlocks.push(allBlocks[i].type.trim() + allBlocks[i].getFieldValue("NAME").trim());
                customAccessBlocks.push(allBlocks[i]);
            }
        }

        $.each(allCustomWorkspaceBlocks, function (key, value) {
            if (!counts.hasOwnProperty(value)) {
                counts[value] = 1;
            } else {
                counts[value] ++;
            }
        });

        for (var k = 0; k < allCustomWorkspaceBlocks.length; k++) {
            if (counts[allCustomWorkspaceBlocks[k].trim()] >= 2) {
                var index = allCustomWorkspaceBlocks.lastIndexOf(allCustomWorkspaceBlocks[k].trim());
                customAccessBlocks[index].dispose();
                break;
            }
        }
    };
    
    getPaper = function () {
        return ballDrawer.getPaper();
    };

    getWs = function () {
        return workspace;
    };

    return {
        initBlockly: initBl,
        getWorkspace: getWs
    }
});