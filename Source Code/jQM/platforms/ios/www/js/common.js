var itemArray = [];
requirejs.config({
    baseUrl: 'js/lib',
    shim: {
        'collection/msgPop': {
            deps: ['collection/jquery', 'collection/raphael']
        },
        'generators/colourgen': {
            deps: ['javascript', 'blocks/colour']
        },
        'generators/buttongen': {
            deps: ['javascript', 'blocks/button']
        },
        'generators/ballgen': {
            deps: ['javascript', 'blocks/ball']
        },
        'generators/cameragen': {
            deps: ['javascript', 'blocks/camera']
        },
        'generators/accelerometergen': {
            deps: ['javascript', 'blocks/accelerometer']
        },
        'generators/listsgen': {
            deps: ['javascript', 'blocks/lists']
        },
        'generators/logicgen': {
            deps: ['javascript', 'blocks/logic']
        },
        'generators/loopsgen': {
            deps: ['javascript', 'blocks/loops']
        }, 
        'generators/interactivegen': {
            deps: ['javascript', 'blocks/interactive']
        },
        'generators/mathgen': {
            deps: ['javascript', 'blocks/math']
        },
        'generators/proceduresgen': {
            deps: ['javascript', 'blocks/procedures']
        },
        'generators/svggen': {
            deps: ['javascript', 'blocks/svg']
        },
        'generators/textsgen': {
            deps: ['javascript', 'blocks/texts']
        },
        'generators/variablesgen': {
            deps: ['javascript', 'blocks/variables']
        },
        'collection/messages': {
            deps: ['javascript']
        }
    }
});
requirejs(['collection/blockly_compressed']);
requirejs(['start']);