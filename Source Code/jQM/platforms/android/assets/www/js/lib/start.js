"use strict";
define(['collection/msgPop', 'generators/colourgen', 'generators/buttongen', 'generators/ballgen', 'generators/cameragen', 'generators/accelerometergen', 'generators/listsgen', 'generators/logicgen', 'generators/loopsgen','generators/interactivegen', 'generators/mathgen', 'generators/proceduresgen', 'generators/svggen', 'generators/textsgen', 'generators/variablesgen', 'collection/messages'], function () {
 requirejs(['app']);
});