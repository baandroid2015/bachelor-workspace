define([], function () {
    var paper;
    var circle;
    var pageWidth;
    var pageHeight;
    var valueX = 0;
    var valueY = 0;
    var bbox;
    var actualPositionX;
    var actualPositionY;
    var ballRadius;

    drawBall = function (id, ballValues) {
        pageWidth = $("#programPage").width();
        pageHeight = $("#programPage").height();
        ballRadius = ballValues[0];
        var desiredXPos = ballValues[1];
        var desiredYPos = ballValues[2];
        var xPos;
        var yPos;
        if (desiredXPos != 0) {
            xPos = desiredXPos / 100 * pageWidth;
        } else {
            xPos = ballRadius;
        }
        if (desiredYPos != 0) {
            yPos = desiredYPos / 100 * pageHeight;
        } else {
            yPos = ballRadius;
        }
        if (xPos <= ballRadius) {
            xPos = ballRadius;
        }
        if (xPos >= pageWidth - ballRadius) {
            xPos = pageWidth - ballRadius;
        }
        if (yPos <= ballRadius) {
            yPos = ballRadius;
        }
        if (yPos >= pageHeight - ballRadius) {
            yPos = pageHeight - ballRadius;
        }
        //paper.canvas.setAttribute('id', 'programPaper');
        circle = paper.circle(xPos, yPos, ballRadius);
        circle.attr("fill", "#00ff00");
        circle.id = id;
        circle.attr("stroke", "#fff");

        //                circle.click(function (evt) {
        //                    circle.attr("fill", "#00ff00");
        //                });

        //showNotification("message", "Tilt device to the right for calibration");
    };

    redrawBall = function (xDir, yDir) {
        bbox = circle.getBBox();
        actualPositionY = bbox.y;
        actualPositionX = bbox.x;
        //setAccelerometerOrientation(acceleration);
        // circle x position
        //circle.attr('cx')
        //oder
        //Math.floor(acceleration.x * (-100))
        // circle.attr('cy'); bzw. circle.attr('cx'); liefern startwerte des Kreises vor Transformation; -->nach Transformation bbox nehmen
        // Tablet nach hinten
        if (actualPositionY < (pageHeight - (ballRadius * 2)) && xDir > 2) {
            valueY += 4;
            // vom Startpunk aus wird der Punkt um x nach rechts und y nach unten verschoben
            circle.transform('"t' + valueX + ',' + valueY + '"');
        }
        // Tablet nach vorne
        if (actualPositionY > 0 && xDir < -2) {
            valueY -= 4;
            circle.transform('"t' + valueX + ',' + valueY + '"');
        }
        // Tablet nach links
        if (actualPositionX > 0 && yDir < -2) {
            valueX -= 4;
            circle.transform('"t' + valueX + ',' + valueY + '"');
        }
        // Tablet nach rechts
        if (actualPositionX < (pageWidth - (ballRadius * 2 + 4)) && yDir > 2) {
            valueX += 4;
            circle.transform('"t' + valueX + ',' + valueY + '"');
        }
        //correction left
        if (actualPositionX < 4 && actualPositionX > 0 && xDir > 2) {
            valueX -= 1;
            circle.transform('"t' + valueX + ',' + valueY + '"');
        }
        //correction right
        if (actualPositionX < (pageWidth - (ballRadius * 2 + 4)) && actualPositionX > (pageWidth - ballRadius) && xDir < -2) {
            valueX += 1;
            circle.transform('"t' + valueX + ',' + valueY + '"');
        }
        //correction bottom
        if (actualPositionY < pageHeight - (ballRadius * 2 + 4) && actualPositionY < (pageHeight - ballRadius) && yDir > 2) {
            valueY += 1;
            circle.transform('"t' + valueX + ',' + valueY + '"');
        }
        //correction top
        if (actualPositionY < 4 && actualPositionY > 0 && yDir < -2) {
            valueY -= 1;
            circle.transform('"t' + valueX + ',' + valueY + '"');
        }
    };

    bounceOfFromEdge = function (pixels) {
        // links
        if (actualPositionX < 2) {
            valueX += pixels;
            circle.transform('"t' + valueX + ',' + valueY + '"');
        }
        // rechts
        if (actualPositionX > pageWidth - ballRadius * 2 - 4) {
            valueX -= pixels;
            circle.transform('"t' + valueX + ',' + valueY + '"');
        }
        // oben
        if (actualPositionY < 2) {
            valueY += pixels;
            circle.transform('"t' + valueX + ',' + valueY + '"');
        }
        // unten
        if (actualPositionY > pageHeight - ballRadius * 2 - 6) {
            valueY -= pixels;
            circle.transform('"t' + valueX + ',' + valueY + '"');
        }
    }

    setPaper = function (p) {
        paper = p;
    }

    getPaper = function () {
        return paper;
    }

    getActualPositionX = function () {
        return actualPositionX;
    }

    getActualPositionY = function () {
        return actualPositionY;
    }

    getPageHeight = function () {
        return pageHeight;
    }

    getPageWidth = function () {
        return pageWidth;
    }

    getBallRadius = function () {
        return ballRadius;
    }
    
    clearAllVar = function () {
    paper = "";
    circle = "";
    pageWidth = "";
    pageHeight = "";
    valueX = 0;
    valueY = 0;
    bbox = "";
    actualPositionX = "";
    actualPositionY = "";
    ballRadius = "";
    }


    return {
        drawBall: drawBall,
        redrawBall: redrawBall,
        getActualPositionX: getActualPositionX,
        getActualPositionY: getActualPositionY,
        bounceOfFromEdge: bounceOfFromEdge,
        getPageHeight: getPageHeight,
        getPageWidth: getPageWidth,
        getBallRadius: getBallRadius,
        setPaper: setPaper,
        getPaper: getPaper,
        clearAllVar: clearAllVar
    }
});




//    redrawBall = function (xDir, yDir) {
//        bbox = circle.getBBox();
//        actualPositionX = bbox.x;
//        actualPositionY = bbox.y;
//        //setAccelerometerOrientation(acceleration);
//        // circle x position
//        //circle.attr('cx')
//        //oder
//        //Math.floor(acceleration.x * (-100))
//        // circle.attr('cy'); bzw. circle.attr('cx'); liefern startwerte des Kreises vor Transformation; -->nach Transformation bbox nehmen
//        // Tablet nach rechts
//        if (actualPositionX < (pageWidth - (ballRadius * 2 + 4)) && xDir < -2) {
//            valueX += 4;
//            // vom Startpunk aus wird der Punkt um x nach rechts und y nach unten verschoben
//            circle.transform('"t' + valueX + ',' + valueY + '"');
//        }
//        // Tablet nach links
//        if (actualPositionX >= 4 && xDir > 2) {
//            valueX -= 4;
//            circle.transform('"t' + valueX + ',' + valueY + '"');
//        }
//        // Tablet nach vorn
//        if (actualPositionY >= 4 && yDir < -2) {
//            valueY -= 4;
//            circle.transform('"t' + valueX + ',' + valueY + '"');
//        }
//        // Tablet nach hinten
//        if (actualPositionY < (pageHeight - (ballRadius * 2 + 4)) && yDir > 2) {
//            valueY += 4;
//            circle.transform('"t' + valueX + ',' + valueY + '"');
//        }
//        //correction left
//        if (actualPositionX < 4 && actualPositionX > 0 && xDir > 2) {
//            valueX -= 1;
//            circle.transform('"t' + valueX + ',' + valueY + '"');
//        }
//        //correction right
//        if (actualPositionX < (pageWidth - (ballRadius * 2 + 4)) && actualPositionX > (pageWidth - ballRadius) && xDir < -2) {
//            valueX += 1;
//            circle.transform('"t' + valueX + ',' + valueY + '"');
//        }
//        //correction bottom
//        if (actualPositionY < pageHeight - (ballRadius * 2 + 4) && actualPositionY < (pageHeight - ballRadius) && yDir > 2) {
//            valueY += 1;
//            circle.transform('"t' + valueX + ',' + valueY + '"');
//        }
//        //correction top
//        if (actualPositionY < 4 && actualPositionY > 0 && yDir < -2) {
//            valueY -= 1;
//            circle.transform('"t' + valueX + ',' + valueY + '"');
//        }
//    };