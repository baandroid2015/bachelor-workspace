'use strict';

goog.provide('Blockly.JavaScript.svg');

goog.require('Blockly.JavaScript');

Blockly.JavaScript['svg_drag'] = function (block) {
    var dropdown_name = block.getFieldValue('NAME');
    var statements_name = Blockly.JavaScript.statementToCode(block, 'NAME');
    var code = 'var mousedown = false,lastX, lastY, path, pathString,colour = "black";$("#programPage").on("touchstart", function (e) {e.preventDefault();' + statements_name + '});';
    return code;
};

Blockly.JavaScript['svg_drawline'] = function (block) {
    var dropdown_name = block.getFieldValue('NAME');
    var code = '(0, eval)("var drawing;");drawing = setTimeout(function(){var paper = ballDrawer.getPaper();e.preventDefault();mousedown = true;var x = e.originalEvent.touches[0].pageX,y = e.originalEvent.touches[0].pageY;pathString = "M" + x + " " + y + "l0 0";path = paper.path(pathString);path.attr({"stroke": colour,"stroke-linecap": "round","stroke-linejoin": "round","stroke-width": 7});lastX = x;lastY = y;$("#programPage").on("touchend", function () {mousedown = false;});$("#programPage").on("touchmove", function (e) {e.preventDefault();if (!mousedown) {return;}var x = e.originalEvent.touches[0].pageX,y = e.originalEvent.touches[0].pageY;pathString += "l" + (x - lastX) + " " + (y - lastY);path.attr("path", pathString);lastX = x;lastY = y;});}, 0);';
    return code;
};

Blockly.JavaScript['svg_clear'] = function (block) {
    var dropdown_name = block.getFieldValue('NAME');
    var code = 'ballDrawer.getPaper().clear();';
    return code;
};
  

Blockly.JavaScript['svg_drawingcol'] = function (block) {
    var dropdown_name = block.getFieldValue('NAME');
    var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
    // TODO: Assemble JavaScript into code variable.
    var code = 'colour = ' + value_name + ';';
    return code;
};

//neuere Version:
//'use strict';
//
//goog.provide('Blockly.JavaScript.svg');
//
//goog.require('Blockly.JavaScript');
//
//Blockly.JavaScript['svg_drag'] = function (block) {
//    var dropdown_name = block.getFieldValue('NAME');
//    var statements_name = Blockly.JavaScript.statementToCode(block, 'NAME');
//    var code = 'var mousedown = false,lastX, lastY, path, pathString,colour = "black";$("#programPage").on("touchstart", function (e) {e.preventDefault();' + statements_name + '});';
//    return code;
//};
//
//Blockly.JavaScript['svg_drawline'] = function (block) {
//    var dropdown_name = block.getFieldValue('NAME');
//    var code = '(0, eval)("var drawing;");try {$("#programPage").off("touchstart");} catch (e) {console.log(e);} drawing = setTimeout(function(){var paper = ballDrawer.getPaper();$("#programPage").on("touchstart", function (e) {e.preventDefault();mousedown = true;var x = e.originalEvent.touches[0].pageX,y = e.originalEvent.touches[0].pageY;pathString = "M" + x + " " + y + "l0 0";path = paper.path(pathString);path.attr({"stroke": colour,"stroke-linecap": "round","stroke-linejoin": "round","stroke-width": 7});lastX = x;lastY = y;});$("#programPage").on("touchend", function () {mousedown = false;});$("#programPage").on("touchmove", function (e) {e.preventDefault();if (!mousedown) {return;}var x = e.originalEvent.touches[0].pageX,y = e.originalEvent.touches[0].pageY;pathString += "l" + (x - lastX) + " " + (y - lastY);path.attr("path", pathString);lastX = x;lastY = y;});}, 0);';
//    return code;
//};
//  
//
//Blockly.JavaScript['svg_drawingcol'] = function (block) {
//    var dropdown_name = block.getFieldValue('NAME');
//    var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
//    // TODO: Assemble JavaScript into code variable.
//    var code = 'colour = ' + value_name + ';';
//    return code;
//};
