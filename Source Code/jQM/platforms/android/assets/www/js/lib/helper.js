    define(['item', 'blockly', 'ballDrawer', 'deviceAccess'], function (item, blockly, ballDrawer, deviceAccess) {

        getUniqueNumber = function (type) {
            var number = [];
            for (var i = 0; i < itemArray.length; i++) {
                if (type == itemArray[i].getProperties().getType().trim()) {
                    number.push(itemArray[i].getProperties().getNumber());
                }
            }
            if (number.length == 0) {
                return 1;
            } else {
                return Math.max.apply(null, number) + 1;
            }
        };

        showLoaderWidget = function () {
            var interval = setInterval(function () {
                $.mobile.loading('show', {
                    text: "Loading...",
                    textVisible: true,
                    theme: "b",
                    textonly: false,
                    html: ""
                });
                clearInterval(interval);
            }, 1);
        };

        hideLoaderWidget = function () {
            var interval = setInterval(function () {
                $.mobile.loading('hide');
                clearInterval(interval);
            }, 1);
        };

        //types: message, error, success
        showNotification = function (type, message) {
            MsgPop.open({
                Type: type,
                Content: message,
                HideCloseBtn: true,
                ShowIcon: false,
                AutoClose: "auto",
                CloseTimer: 1750
            });
        };

        //splice(x,y): start at index x and take from there y objects away
        deleteItemInItemArray = function (uniqueName) {
            for (var i = 0; i < itemArray.length; i++) {
                if (itemArray[i].getProperties().getUniqueName() == uniqueName.trim()) {
                    itemArray.splice(i, 1);
                }
            }
        };

        //create loading list
        createLoadingList = function () {
            for (var i = 0; i < window.localStorage.length; i++) {
                var key = window.localStorage.key(i);
                /* if (window.localStorage.getItem(prog).startsWith("<xml")) {
                     $('<option>').attr({
                         'value': i + 1
                     }).html(prog).appendTo('#load-choices');
                 }*/
                $("#loadinglist").append('<li><a href="#savedelpage" data-transition="slide" class="restoreOrDelete">' + key + '</a</li>').listview().listview('refresh');
            }
            $("#loadingHeader").html("There is/are " + window.localStorage.length + " program(s) available");
        };

        isProgramInLocalStorage = function (name) {
            var bool = false;
            for (var i = 0; i < window.localStorage.length; i++) {
                if (window.localStorage.key(i).trim() == name.trim()) {
                    bool = true;
                }
            }
            return bool;
        };

        saveProgram = function (currentProgramName, kind) {
            if (currentProgramName != null && currentProgramName.trim() != "") {
                if (!isProgramInLocalStorage(currentProgramName) || kind == "save") {
                    //Blockly Workspace to XML
                    var xmlDom = Blockly.Xml.workspaceToDom(blockly.getWorkspace());
                    var workspaceXml = Blockly.Xml.domToPrettyText(xmlDom);
                    var programToLocalStorage = [];
                    //write Items to Array
                    for (var i = 0; i < itemArray.length; i++) {
                        if (itemArray[i].getProperties().getType() == "Button") {
                            programToLocalStorage.push({
                                number: itemArray[i].getProperties().getNumber(),
                                art: itemArray[i].getProperties().getType(),
                                colour: itemArray[i].getProperties().getButtonColour()
                            });
                        } else if (itemArray[i].getProperties().getType() == "Ball") {
                            programToLocalStorage.push({
                                number: itemArray[i].getProperties().getNumber(),
                                art: itemArray[i].getProperties().getType(),
                                ballValues: JSON.stringify(itemArray[i].getProperties().getBallValues())
                            });
                        } else if (itemArray[i].getProperties().getType() == "ScalableVectorGraphics") {
                            programToLocalStorage.push({
                                number: itemArray[i].getProperties().getNumber(),
                                art: itemArray[i].getProperties().getType()
                            });
                        } else if (itemArray[i].getProperties().getType() == "Accelerometer") {
                            programToLocalStorage.push({
                                number: itemArray[i].getProperties().getNumber(),
                                art: itemArray[i].getProperties().getType(),
                                accelRate: itemArray[i].getProperties().getAccelRate()
                            });
                        } else if (itemArray[i].getProperties().getType() == "Camera") {
                            programToLocalStorage.push({
                                number: itemArray[i].getProperties().getNumber(),
                                art: itemArray[i].getProperties().getType(),
                                snapQuali: itemArray[i].getProperties().getSnapQuali()
                            });
                        }
                    }
                    //write Blockly XMl to Array
                    programToLocalStorage.push({
                        blockly: workspaceXml
                    });
                    //save Array in Local Storage
                    window.localStorage.setArray(currentProgramName, programToLocalStorage);
                    // append to Loading List
                    if (kind != "save") {
                        $("#loadinglist").append('<li><a href="#savedelpage" data-transition="slide" class="restoreOrDelete">' + currentProgramName + '</a></li>').listview().listview('refresh');
                        $("#loadingHeader").html("There is/are " + window.localStorage.length + " program(s) available");
                    }
                    $("#myownpanel").panel('close');
                    showNotification("success", "'" + currentProgramName + "' was saved successfully!");
                    setProgramInHeader(currentProgramName);
                    removeSaveFromPanel();
                    addSaveToPanel();
                } else {
                    showNotification("error", "Could not save. There is already a program with the same name!");
                    $("#myownpanel").panel('close');
                }
            } else {
                showNotification("error", "Could not save. Please enter a valid name!");
                $("#myownpanel").panel('close');
            }
        };

        restoreProgram = function (currentName) {
            $("#deleteCurrentWorksapce").trigger("tap");
            // get program array out of localstorage
            var program = window.localStorage.getArray(currentName);
            // get blocklyXml out of Array
            var blocklyXml = program[program.length - 1].blockly;
            // remove blockly Xml
            program.pop();
            // write loaded Blockly into workspace
            var xmlToDom = Blockly.Xml.textToDom(blocklyXml);
            try {
                Blockly.Xml.domToWorkspace(blockly.getWorkspace(), xmlToDom);
            } catch (e) {
                alert("Could not write domToWorkspace! " + e);
            }
            //            // clear items
            //            clearDivs();
            //            itemArray = [];
            //draw new Items
            var allTypes = getAllItemArrayTypes();
            if ($.inArray("Accelerometer", allTypes) != -1) {

            }
            for (var i = 0; i < program.length; i++) {
                if (program[i].art.trim() == "Ball") {
                    itemArray.push(new item(program[i].number, program[i].art));
                    var ballVal = JSON.parse(program[i].ballValues);
                    itemArray[itemArray.length - 1].getProperties().setBallValues(ballVal[0], ballVal[1], ballVal[2]);
                    itemArray[itemArray.length - 1].drawItem();
                } else if (program[i].art.trim() == "ScalableVectorGraphics") {
                    itemArray.push(new item(program[i].number, program[i].art));
                    itemArray[itemArray.length - 1].drawItem();
                } else if (program[i].art.trim() == "Accelerometer") {
                    itemArray.push(new item(program[i].number, program[i].art));
                    itemArray[itemArray.length - 1].getProperties().setAccelRate(program[i].accelRate);
                    deviceAccess.setSamplingRate(program[i].accelRate);
                    itemArray[itemArray.length - 1].drawItem();
                } else if (program[i].art.trim() == "Button") {
                    itemArray.push(new item(program[i].number, program[i].art));
                    itemArray[itemArray.length - 1].getProperties().setButtonColour(program[i].colour);
                    itemArray[itemArray.length - 1].drawItem();
                } else if (program[i].art.trim() == "Camera") {
                    itemArray.push(new item(program[i].number, program[i].art));
                    itemArray[itemArray.length - 1].getProperties().setSnapQuali(program[i].snapQuali);
                    itemArray[itemArray.length - 1].drawItem();
                }
            }
            showNotification("success", "'" + currentName + "' successfully restored!");
            setProgramInHeader(currentName);
            refreshCustomCategories();
            resetPropertiesSliders();
            removeSaveFromPanel();
            addSaveToPanel();
            //rerenderBlocks();  
        };

        deleteCurrentWorkspace = function () {
            clearDivs();
            itemArray = [];
            blockly.getWorkspace().clear();
            setProgramInHeader(" ");
            removeSaveFromPanel();
            showNotification("success", "Workspace was successfully cleared!");
            ballDrawer.clearAllVar();
            deleteAllCustomCategories();
            $("#myownpanel").panel('close');
        };

        clearDivs = function () {
            $("#pageOneDiv").empty();
            $('#controlBtns').empty();
            $('#program').empty();
            var btn = $('#programPage .btnDetach').detach();
            var img = $('#programPage .imgDetach').detach();
            $('#programPage').empty();
            $('#programPage').append(btn);
            $('#programPage').append(img);
            $('#program').append('<img id="appImage" src="" />');
        };

        setProgramInHeader = function (progName) {
            $("#headerMad").html("Tap4App");
            $("#headerChAnItem").html("Choose an item");
            $("#headerYourProg").html("Your Program");
            $("#headerBlockEdit").html("Block Editor");
            $("#headerMad").append(" [" + progName + "]");
            $("#headerChAnItem").append(" [" + progName + "]");
            $("#headerYourProg").append(" [" + progName + "]");
            $("#headerBlockEdit").append(" [" + progName + "]");
        };

        getAllItemArrayTypes = function () {
            var typesArray = [];
            for (var i = 0; i < itemArray.length; i++) {
                typesArray.push(itemArray[i].getProperties().getType().trim());
            }
            return typesArray;
        };

        getUniqueNamesByType = function (type) {
            var uniqueNames = [];
            for (var i = 0; i < itemArray.length; i++) {
                if (itemArray[i].getProperties().getType() == type.trim()) {
                    uniqueNames.push(itemArray[i].getProperties().getUniqueName());
                }
            }
            return uniqueNames;
        };

        addSaveToPanel = function () {
            $('<a href="#" data-role="button" data-theme="b" data-icon="check" id="save">Save</a>').insertAfter("#saveAs");
            $("#myownpanel").panel().enhanceWithin();
        };

        removeSaveFromPanel = function () {
            $("#save").remove();
        };


        rerenderBlocks = function () {
            var allBlocks = blockly.getWorkspace().getAllBlocks();
            for (var i = 0; i < allBlocks.length; i++) {
                allBlocks[i].render();
            }
        };

        refreshCustomCategories = function () {
            var typesArray = [];
            typesArray = getAllItemArrayTypes();
            typesArray = $.unique(typesArray);
            if (typesArray.length != 0) {
                for (var i = 0; i < typesArray.length; i++) {
                    //$.inArray: if value is not found, -1 will be returned

                    // Button
                    //Button im ItemArray aber nicht in Toolbox
                    if ($.inArray("Button", typesArray) != -1 && $("#toolbox").find('category[name="Button"]').html() == undefined) {
                        $("#toolbox").append('<sep name="ButtonSep"></sep>');
                        $("#toolbox").append('<category name="Button"><block type="button_click"></block></category>');
                        // Button nicht in ItemArray aber in Toolbox
                    } else if ($.inArray("Button", typesArray) == -1 && $("#toolbox").find('category[name="Button"]').html() != undefined) {
                        $("#toolbox").find('category[name="Button"]').remove();
                        $("#toolbox").find('sep[name="ButtonSep"]').remove();
                    }
                    // Ball
                    // Ball im Array aber nicht in Toolbox
                    if ($.inArray("Ball", typesArray) != -1 && $("#toolbox").find('category[name="Ball"]').html() == undefined) {
                        $("#toolbox").append('<sep name="BallSep"></sep>');
                        $("#toolbox").append('<category name="Ball"><block type="ball_clicked"></block><block type="ball_edge"></block><block type="ball_move"></block><block type="ball_setcolour"></block><block type="ball_edgebounce"></block></block></category>');
                        // Ball nicht in Array aber in Toolbox
                    } else if ($.inArray("Ball", typesArray) == -1 && $("#toolbox").find('category[name="Ball"]').html() != undefined) {
                        $("#toolbox").find('category[name="Ball"]').remove();
                        $("#toolbox").find('sep[name="BallSep"]').remove();
                    }

                    //Accelerometer
                    if ($.inArray("Accelerometer", typesArray) != -1 && $("#toolbox").find('category[name="Accelerometer"]').html() == undefined) {
                        $("#toolbox").append('<sep name="AccelSep"></sep>');
                        $("#toolbox").append('<category name="Accelerometer"><block type="accel_xvalue"></block><block type="accel_yvalue"></block></category>');
                    } else if ($.inArray("Accelerometer", typesArray) == -1 && $("#toolbox").find('category[name="Accelerometer"]').html() != undefined) {
                        $("#toolbox").find('category[name="Accelerometer"]').remove();
                        $("#toolbox").find('sep[name="AccelSep"]').remove();
                    }

                    //ScalableVectorGraphics
                    if ($.inArray("ScalableVectorGraphics", typesArray) != -1 && $("#toolbox").find('category[name="SVG"]').html() == undefined) {
                        $("#toolbox").append('<sep name="svgSep"></sep>');
                        $("#toolbox").append('<category name="SVG"><block type="svg_drag"></block><block type="svg_drawline"></block><block type="svg_drawingcol"></block><block type="svg_clear"></block></category>');
                    } else if ($.inArray("ScalableVectorGraphics", typesArray) == -1 && $("#toolbox").find('category[name="SVG"]').html() != undefined) {
                        $("#toolbox").find('category[name="SVG"]').remove();
                        $("#toolbox").find('sep[name="svgSep"]').remove();
                    }

                    //Camera
                    if ($.inArray("Camera", typesArray) != -1 && $("#toolbox").find('category[name="Camera"]').html() == undefined) {
                        $("#toolbox").append('<sep name="camSep"></sep>');
                        $("#toolbox").append('<category name="Camera"><block type="cam_getsnap"></block><block type="cam_setrear"></block></category>');
                    } else if ($.inArray("Camera", typesArray) == -1 && $("#toolbox").find('category[name="Camera"]').html() != undefined) {
                        $("#toolbox").find('category[name="Camera"]').remove();
                        $("#toolbox").find('sep[name="camSep"]').remove();
                    }
                }
            } else {
                deleteAllCustomCategories();
            }
            blockly.getWorkspace().updateToolbox(document.getElementById("toolbox"));
        };

        deleteAllCustomCategories = function () {
            //var lastCategory = $("#toolbox").find('category:last').attr("name").trim();
            $("#toolbox").find('category[name="Button"]').remove();
            $("#toolbox").find('sep[name="ButtonSep"]').remove();
            $("#toolbox").find('category[name="Accelerometer"]').remove();
            $("#toolbox").find('sep[name="AccelSep"]').remove();
            $("#toolbox").find('category[name="Ball"]').remove();
            $("#toolbox").find('sep[name="BallSep"]').remove();
            $("#toolbox").find('category[name="SVG"]').remove();
            $("#toolbox").find('sep[name="svgSep"]').remove();
            $("#toolbox").find('category[name="Camera"]').remove();
            $("#toolbox").find('sep[name="camSep"]').remove();
            blockly.getWorkspace().updateToolbox(document.getElementById("toolbox"));
        };

        resetPropertiesSliders = function () {
            if ($("#ballSize").attr('class') != undefined) {
                $("#ballSize").val("20");
                $("#ballSize").slider("refresh");
            }
            if ($("#xpos").attr('class') != undefined) {
                $("#xpos").val("50");
                $("#xpos").slider("refresh");
            }
            if ($("#ypos").attr('class') != undefined) {
                $("#ypos").val("50");
                $("#ypos").slider("refresh");
            }
            if ($("#accelRate").attr('class') != undefined) {
                $("#accelRate").val("10");
                $("#accelRate").slider("refresh");
            }
        };

        isItemAllowed = function (type) {
            var types = getAllItemArrayTypes();
            var allowed = true;

            if (type == "Button") {
                return allowed;
            }
            if (type == "Ball" && $.inArray("Ball", types) == -1 && $.inArray("ScalableVectorGraphics", types) != -1) {
                return allowed;
            } else if (type == "Ball" && $.inArray("Ball", types) != -1) {
                allowed = false;
                return allowed;
            }
            if (type == "ScalableVectorGraphics" && $.inArray("ScalableVectorGraphics", types) == -1) {
                return allowed;
            } else if (type == "ScalableVectorGraphics" && $.inArray("ScalableVectorGraphics", types) != -1) {
                allowed = false;
                return allowed;
            }
            if (type == "Accelerometer" && $.inArray("Accelerometer", types) == -1) {
                return allowed;
            } else if (type == "Accelerometer" && $.inArray("Accelerometer", types) != -1) {
                allowed = false;
                return allowed;
            }
            if (type == "Camera" && $.inArray("Camera", types) == -1) {
                return allowed;
            } else if (type == "Camera" && $.inArray("Camera", types) != -1) {
                allowed = false;
                return allowed;
            }
        };

        Storage.prototype.setArray = function (key, obj) {
            return this.setItem(key, JSON.stringify(obj))
        }

        Storage.prototype.getArray = function (key) {
            return JSON.parse(this.getItem(key))
        }

        return {
            getUniqueNumber: getUniqueNumber,
            showLoaderWidget: showLoaderWidget,
            hideLoaderWidget: hideLoaderWidget,
            showNotification: showNotification,
            deleteItemInItemArray: deleteItemInItemArray,
            createLoadingList: createLoadingList,
            isProgramInLocalStorage: isProgramInLocalStorage,
            saveProgram: saveProgram,
            setProgramInHeader: setProgramInHeader,
            addSaveToPanel: addSaveToPanel,
            removeSaveFromPanel: removeSaveFromPanel,
            rerenderBlocks: rerenderBlocks,
            restoreProgram: restoreProgram,
            deleteCurrentWorkspace: deleteCurrentWorkspace,
            refreshCustomCategories: refreshCustomCategories,
            deleteAllCustomCategories: deleteAllCustomCategories,
            getAllItemArrayTypes: getAllItemArrayTypes,
            getUniqueNamesByType: getUniqueNamesByType,
            resetPropertiesSliders: resetPropertiesSliders,
            isItemAllowed: isItemAllowed,
            clearDivs: clearDivs
        }

    });