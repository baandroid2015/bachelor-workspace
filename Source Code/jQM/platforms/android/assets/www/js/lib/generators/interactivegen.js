'use strict';

goog.provide('Blockly.JavaScript.interactive');

goog.require('Blockly.JavaScript');

Blockly.JavaScript['xtouch'] = function(block) {
  var code = 'deviceAccess.printXTouch();';
  return code;
};

Blockly.JavaScript['ytouch'] = function(block) {
  var code = 'deviceAccess.printYTouch();';
  return code;
};
