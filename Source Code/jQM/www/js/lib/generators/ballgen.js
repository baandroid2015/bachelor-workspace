'use strict';
goog.provide('Blockly.JavaScript.ball');

goog.require('Blockly.JavaScript');
var dropdown_name;
Blockly.JavaScript['ball_clicked'] = function (block) {
    dropdown_name = block.getFieldValue('NAME');
    var statements_ball_statement = Blockly.JavaScript.statementToCode(block, 'ball_statement');
    var code = 'var ' + dropdown_name + 'cir = ballDrawer.getPaper().getById("' + dropdown_name + '");' + dropdown_name + 'cir.unclick();' + dropdown_name + 'cir.click(function (evt) {' + statements_ball_statement + '});';
    return code;
};

Blockly.JavaScript['ball_edge'] = function (block) {
    var dropdown_name = block.getFieldValue('NAME');
    var statements_name = Blockly.JavaScript.statementToCode(block, 'NAME');
    var code = 'if(ballDrawer.getActualPositionX() < 2 || ballDrawer.getActualPositionX() > (ballDrawer.getPageWidth() - ballDrawer.getBallRadius()*2-3) || ballDrawer.getActualPositionY() < 2 || ballDrawer.getActualPositionY() > (ballDrawer.getPageHeight() - ballDrawer.getBallRadius()*2-4)){' + statements_name + ' }';
    return code;
};

//Blockly.JavaScript['ball_accelerometer'] = function (block) {
//    var dropdown_name = block.getFieldValue('NAME');
//    var statements_accel_statement = Blockly.JavaScript.statementToCode(block, 'accel_statement');
//    // TODO: Assemble JavaScript into code variable.
//    var code = '...';
//    return code;
//};

Blockly.JavaScript['ball_move'] = function (block) {
    var dropdown_name = block.getFieldValue('NAME');
    var value_valuex = Blockly.JavaScript.valueToCode(block, 'valueX', Blockly.JavaScript.ORDER_ATOMIC);
    var value_valuey = Blockly.JavaScript.valueToCode(block, 'valueY', Blockly.JavaScript.ORDER_ATOMIC);
    // TODO: Assemble JavaScript into code variable.
    //    var code = '(0,eval)("var ballMove;"); deviceAccess.startStopAccelerometerWatch("start", 10);ballMove = setInterval(function(){ballDrawer.redrawBall(' + value_valuex + ',' + value_valuey + ');},deviceAccess.getSamplingRate());';
    var code = 'deviceAccess.startStopAccelerometerWatch("start", deviceAccess.getSamplingRate());ballDrawer.redrawBall(' + value_valuex + ',' + value_valuey + ');';

    return code;
};

Blockly.JavaScript['ball_setcolour'] = function (block) {
    var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
    var code = dropdown_name + 'cir.attr("fill",' + value_name + ');';
    return code;
};

Blockly.JavaScript['ball_edgebounce'] = function (block) {
    var dropdown_name = block.getFieldValue('NAME');
    var dropdown_pixels = block.getFieldValue('pixels');
    var code = 'ballDrawer.bounceOfFromEdge(' + dropdown_pixels + ');';
    return code;
};

//var getBallitems = (function () {
//    var ballItems = [];
//    var init = true;
//    var returnValue;
//    return function () {
//        if (init) {
//            for (var i = 0; i < itemArray.length; i++) {
//                if (itemArray[i].getProperties().getType().trim() == "Ball") {
//                    ballItems.push(itemArray[i].getProperties().getUniqueName().trim());
//                }
//            }
//            init = false;
//        }
//        returnValue = ballItems[ballItems.length - 1].trim();
//        ballItems.pop();
//        return returnValue;
//    }
//})();