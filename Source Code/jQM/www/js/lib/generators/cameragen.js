'use strict';

goog.provide('Blockly.Blocks.camera');

goog.require('Blockly.Blocks');

Blockly.JavaScript['cam_getsnap'] = function (block) {
    var dropdown_name = block.getFieldValue('NAME');
    var code = 'deviceAccess.getSnap()';
    return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.JavaScript['cam_setrear'] = function (block) {
    var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
    var code = 'var image = document.getElementById("appImage"); var deferred = ' + value_name + ';$.when(deferred).done(functionC); function functionC(data) {image.src = data;}';
    return code;};

// working
//'use strict';
//
//goog.provide('Blockly.Blocks.camera');
//
//goog.require('Blockly.Blocks');
//
//Blockly.JavaScript['cam_getsnap'] = function (block) {
//    var dropdown_name = block.getFieldValue('NAME');
//    var code = 'navigator.camera.getPicture(onSuccess, onFail, {quality: deviceAccess.getSnapQuali(),destinationType: Camera.DestinationType.DATA_URL});function onSuccess(imageData) {var image = document.getElementById("smallImage");image.src = "data:image/jpeg;base64," + imageData;}function onFail(message) {alert("Failed because: " + message);}';
//    return code;
//};