"use strict";
// ball clicked: https://blockly-demo.appspot.com/static/demos/blockfactory/index.html#jtowgn
//ball set colour: https://blockly-demo.appspot.com/static/demos/blockfactory/index.html#t7bdun
//if accel is accelerated: https://blockly-demo.appspot.com/static/demos/blockfactory/index.html#bbbxce
//accel x value: https://blockly-demo.appspot.com/static/demos/blockfactory/index.html#f6ve3o
//ball edge: https://blockly-demo.appspot.com/static/demos/blockfactory/index.html#6zpgrd
//ball edgebounce: https://blockly-demo.appspot.com/static/demos/blockfactory/index.html#na5sgs
//repeat while true: https://blockly-demo.appspot.com/static/demos/blockfactory/index.html#y2nndx
//svg drag: https://blockly-demo.appspot.com/static/demos/blockfactory/index.html#634jmh
//svg draw line: https://blockly-demo.appspot.com/static/demos/blockfactory/index.html#q9nrdy
//ball xTouch: https://blockly-demo.appspot.com/static/demos/blockfactory/index.html#8wuphu
//Block löschen: var kkk = Blockly.mainWorkspace.getAllBlocks(); kkk[0].dispose();
//Block DropdownValue bekommen: kkk[0].getFieldValue('NAME');

define(['item', 'helper', 'blockly', 'deviceAccess', 'ballDrawer'], function (item, helper, blockly, deviceAccess, ballDrawer) {
    require(["collection/jquerymobile"]);
    // http://www.jqueryscript.net/other/Growl-like-Toast-Notification-Plugin-with-jQuery-Font-Awesome-MsgPop.html
    MsgPop.displaySmall = false
    var allowed;
    var selectedType;;
    var handlerModule = function () {
        $(document).on("pagecreate", "#pageone", function () {
//        $(document).on("pageinit", function () {
            var currentName;
            $("#myownpanel").panel().enhanceWithin();
            blockly.initBlockly();
            helper.showNotification("message", "Welcome to mobile App developer!");
            helper.createLoadingList();
            helper.setProgramInHeader(" ");
            //helper.refreshCustomCategories();

            $(".itemtoselect").on("tap", function () {
                selectedType = $(this).text().trim();
                allowed = helper.isItemAllowed(selectedType);
                //itemArray.push(new item(guidHelper.createNewGuid(), $(this).text().trim()));
                //new item(uniqueNumber, type);
            });

            $(".okbutton").on("tap", function () {
                if (allowed) {
                    itemArray.push(new item(helper.getUniqueNumber(selectedType), selectedType));
                    itemArray[itemArray.length - 1].setProperties();
                    itemArray[itemArray.length - 1].drawItem();
                    helper.refreshCustomCategories();
                    helper.resetPropertiesSliders();
                } else if (allowed == undefined) {
                    helper.showNotification("error", "A ball must live in a SVG!");
                } else {
                    helper.showNotification("error", "You can't an item of this type");
                }
            });

            $(document).on("tap", "#clickDeleteItem", function () {
                var id = $(this).parents('div:eq(1)').attr('id');
                var allBlocks = blockly.getWorkspace().getAllBlocks();
                // delete in itemArray
                helper.deleteItemInItemArray(id);
                //delete on programPage
                $("#" + id.trim() + "-Program").remove();
                $("#" + id.trim() + " ").remove();
                try {
                    var circle = ballDrawer.getPaper().getById(id.trim());
                    circle.remove();
                } catch (e) {
                    console.warn("Warning! No Ball!");
                }
                //delete on pageOne
                $(this).parents('div:eq(1)').remove();
                // delete blocks with selected item in dropdown
                for (var i = 0; i < allBlocks.length; i++) {
                    if (allBlocks[i].getFieldValue("NAME") != null && allBlocks[i].getFieldValue("NAME").trim() == id.trim()) {
                        allBlocks[i].dispose();
                    } else if (allBlocks[i].getFieldValue("DRAWCOL") != null && allBlocks[i].getFieldValue("DRAWCOL").trim() == id.trim()) {
                        allBlocks[i].dispose();
                    }
                }
                $("#pageOneDiv").children().collapsible('collapse');
                helper.refreshCustomCategories();
                helper.showNotification("message", id + " (and blocks using " + id + ") deleted!");
            });

            $(document).on("click", "#save", function () {
                helper.saveProgram(currentName, "save");
            });

            // save created program in Local Storage
            $("#saveAs").on("tap", function () {
                //show name prompt
                currentName = prompt("Please enter the name of the program...");
                helper.saveProgram(currentName, "saveAs");
            });

            $("#deleteCurrentWorksapce").on("tap", function () {
                helper.deleteCurrentWorkspace();
            });

            $(document).on("tap", "#deleteThisProgram", function () {
                var name = $(this).text().trim().slice(6).trim();
                window.localStorage.removeItem(name);
                $("#loadinglist").html("");
                helper.createLoadingList();
                if (name == currentName) {
                    helper.setProgramInHeader("");
                    helper.removeSaveFromPanel();
                }
                helper.showNotification("success", "'" + name + "' successfully deleted!");

            });

            $(document).on("tap", "#restoreThisProgram", function () {
                // get name of program to load
                var key = $(this).text().trim().slice(7).trim();
                currentName = key;
                helper.restoreProgram(currentName);
            });

            // delete or restore a program
            $(document).on("tap", ".restoreOrDelete", function () {
                $("#saveOrDel").html("");
                var name = $(this).text().trim();
                $("#saveOrDel").append('<li data-icon="delete"><a id="deleteThisProgram" class="delOrSave" href="#loadingpage" data-transition="slide"> Delete ' + name + '</a></li>').listview().listview('refresh');
                $("#saveOrDel").append('<li data-icon="recycle"><a id="restoreThisProgram" class="delOrSave" href="#pageone" data-transition="slide"> Restore ' + name + '</a></li>').listview().listview('refresh');
            });

            String.prototype.startsWith = function (prefix) {
                return this.indexOf(prefix) === 0;
            }

            $(document).on("tap", ".show-page-loading-msg", function () {
                helper.showLoaderWidget();
            });

            $(document).on('pageshow', '#blockly', function () {
                blockly.getWorkspace().updateToolbox(document.getElementById("toolbox"));
                //$("#blockly").show();
                helper.hideLoaderWidget();
                helper.rerenderBlocks();
            });

            $("#magicHappens").on("tap", function () {
                helper.showNotification("message", "Press 'back'-Button to go back");
                try {
                    Blockly.JavaScript.addReservedWords('code');
                    var code = Blockly.JavaScript.workspaceToCode(blockly.getWorkspace());
                } catch (e) {
                    alert(e);
                }
                try {
                    eval(code);
                } catch (e) {
                    alert(e);
                }
            });

            $(document).on('pageshow', '#programPage, #loadingpage, #pagetwo, #properties', function () {
                //$("#blockly").hide();
                helper.hideLoaderWidget();
                $("#pageOneDiv").children().collapsible('collapse');
            });

            $(document).on('pageshow', '#pageone', function () {
                helper.hideLoaderWidget();
            });

            $(document).on('pagehide', '#pagetwo', function () {
                //$("#blockly").hide();
                $(".itemsToChoose").collapsible('collapse');
            });

            $(document).on('pagehide', '#programPage', function () {
                deviceAccess.startStopAccelerometerWatch("stop", null);

                // removes touchListeners(drawing)
                try {
                    $("#programPage").off("touchstart");
                    $("#programPage").off("touchend");
                    $("#programPage").off("touchmove");
                    clearTimeout(drawing);
                    drawing = "";
                } catch (e) {
                    console.warn("No touch event(s) to remove: " + e);
                }

                // terminate infinite loop
                try {
                    clearInterval(whileTrue);
                    whileTrue = "";
                } catch (e) {
                    console.warn("No infinite loop to clear: " + e);
                }

                // remove click listeners from Button
                try {
                    var buttonUniqueNames = helper.getUniqueNamesByType("Button");
                    for (var i = 0; i < buttonUniqueNames.length; i++) {
                        $("#" + buttonUniqueNames[i] + "-Program").off("tap");
                    }
                } catch (e) {
                    console.warn("No tap event(s) to remove: " + e);
                }

                /// remove click listener from Ball
                try {
                    var Ball1cir = ballDrawer.getPaper().getById("Ball1");
                    Ball1cir.unclick();
                } catch (e) {
                    console.warn("No Ball to remove listener: " + e);
                }
            });

            $("#testButtonTwo").on("tap", function () {
            })
        });
    }();
    return handlerModule;
});