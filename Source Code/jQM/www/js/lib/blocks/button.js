'use strict';

goog.provide('Blockly.Blocks.button');

goog.require('Blockly.Blocks');

Blockly.Blocks['button_click'] = {
    init: function () {
        this.setHelpUrl('http://www.example.com/');
        this.setColour(345);
        this.appendDummyInput()
            .appendField("If")
            .appendField(new Blockly.FieldDropdown(createButtonDropdownString), "NAME")
            .appendField(".isClicked");
        this.appendStatementInput("statement")
            .setCheck("null")
            .appendField("do");
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setTooltip('');
    }
};

function createButtonDropdownString() {
    var dropdownOptions = [];
    if (itemArray.length != 0) {
        for (var i = 0; i < itemArray.length; i++) {
            //dropdownOptions.push([itemArray[i].getProperties().getName(), itemArray[i].getProperties().getGuid()]);
            if (itemArray[i].getProperties().getType().trim() == "Button") {
                dropdownOptions.push([itemArray[i].getProperties().getUniqueName(), itemArray[i].getProperties().getUniqueName()]);
            }
        }
        return dropdownOptions;
    } else {
        return "-";
    }
}