'use strict';
goog.provide('Blockly.Blocks.ball');

goog.require('Blockly.Blocks');

Blockly.Blocks['ball_clicked'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("If")
            .appendField(new Blockly.FieldDropdown(createBallDropdown), "NAME")
            .appendField("is clicked");
        this.appendStatementInput("ball_statement")
            .setCheck("String")
            .appendField("do");
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setColour(90);
        this.setTooltip('');
        this.data = 'myBallClickedData';
        this.setHelpUrl('http://www.example.com/');
    }
};

Blockly.Blocks['ball_edge'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("If")
        .appendField(new Blockly.FieldDropdown(createBallDropdown), "NAME")
        .appendField("touches border");
    this.appendStatementInput("NAME")
        .setCheck("Text")
        .appendField("do");
    this.setInputsInline(false);
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setColour(90);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Blocks['ball_move'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Move")
            .appendField(new Blockly.FieldDropdown(createBallDropdown), "NAME")
            .appendField("to position");
        this.appendValueInput("valueX")
            .appendField("                                        x")
            .setCheck(['xAccel', 'yAccel']);
        this.appendValueInput("valueY")
            .appendField("                                        y")
            .setCheck(['yAccel', 'xAccel']);
        this.setInputsInline(false);
        this.setPreviousStatement(true, 'abcde');
        this.setNextStatement(true);
        this.setColour(90);
        this.setTooltip('');
        this.setHelpUrl('http://www.example.com/');
    }
};

Blockly.Blocks['ball_setcolour'] = {
    init: function () {
        this.appendDummyInput()
            .appendField(new Blockly.FieldDropdown(createBallDropdown), "NAME");
        this.appendValueInput("NAME")
            .setCheck("Colour")
            .appendField(".set colour to");
        this.setInputsInline(true);
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setColour(90);
        this.setTooltip('');
        this.setHelpUrl('http://www.example.com/');
    }
};

Blockly.Blocks['ball_edgebounce'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldDropdown(createBallDropdown), "NAME")
        .appendField("bounce")
        .appendField(new Blockly.FieldDropdown([["15px", "15"], ["30px", "30"], ["45px", "45"]]), "pixels")
        .appendField("off from border");
    this.setInputsInline(true);
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setColour(90);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

function createBallDropdown() {
    var dropdownOptions = [];
    if (itemArray.length != 0) {
        for (var i = 0; i < itemArray.length; i++) {
            //dropdownOptions.push([itemArray[i].getProperties().getName(), itemArray[i].getProperties().getGuid()]);
            if (itemArray[i].getProperties().getType().trim() == "Ball") {
                dropdownOptions.push([itemArray[i].getProperties().getUniqueName(), itemArray[i].getProperties().getUniqueName()]);
            }
        }
        if (dropdownOptions.length != 0) {
            return dropdownOptions;
        } else {
            return "-";
        }
    } else {
        return "-";
    }
}