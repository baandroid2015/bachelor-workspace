'use strict';

goog.provide('Blockly.Blocks.interactive');

goog.require('Blockly.Blocks');

Blockly.Blocks['xtouch'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("print 'x' value of touch position");
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setColour(270);
        this.setTooltip('');
        this.setHelpUrl('http://www.example.com/');
    }
};

Blockly.Blocks['ytouch'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("print 'y' value of touch position");
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setColour(270);
        this.setTooltip('');
        this.setHelpUrl('http://www.example.com/');
    }
};