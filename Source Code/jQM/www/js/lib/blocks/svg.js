'use strict';

goog.provide('Blockly.Blocks.svg');

goog.require('Blockly.Blocks');

Blockly.Blocks['svg_drag'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("When ")
            .appendField(new Blockly.FieldDropdown(createSVGDropdown), "NAME")
            .appendField(".dragged");
        this.appendStatementInput("NAME")
            .appendField("do")
            .setCheck('drawline');
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setInputsInline(false);
        this.setColour(300);
        this.setTooltip('');
        this.setHelpUrl('http://www.example.com/');
    }
};

Blockly.Blocks['svg_drawline'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("call")
            .appendField(new Blockly.FieldDropdown(createSVGDropdown), "NAME")
            .appendField(".drawLine");
        this.setInputsInline(false);
        this.setPreviousStatement(true, 'drawline');
        this.setNextStatement(true);
        this.setColour(300);
        this.setTooltip('');
        this.setHelpUrl('http://www.example.com/');
    }
};

Blockly.Blocks['svg_clear'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("call")
            .appendField(new Blockly.FieldDropdown(createSVGDropdown), "NAME")
            .appendField(".clear");
        this.setInputsInline(false);
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setColour(300);
        this.setTooltip('');
        this.setHelpUrl('http://www.example.com/');
    }
};

Blockly.Blocks['svg_drawingcol'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("set")
            .appendField(new Blockly.FieldDropdown(createSVGDropdown), "DRAWCOL")
            .appendField("drawing colour to");
        this.appendValueInput("NAME")
            .setCheck("Colour");
        this.setInputsInline(true);
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setColour(300);
        this.setTooltip('');
        this.setHelpUrl('http://www.example.com/');
    }
};


function createSVGDropdown() {
    var dropdownOptions = [];
    if (itemArray.length != 0) {
        for (var i = 0; i < itemArray.length; i++) {
            //dropdownOptions.push([itemArray[i].getProperties().getName(), itemArray[i].getProperties().getGuid()]);
            if (itemArray[i].getProperties().getType().trim() == "ScalableVectorGraphics") {
                dropdownOptions.push([itemArray[i].getProperties().getUniqueName(), itemArray[i].getProperties().getUniqueName()]);
            }
        }
        if (dropdownOptions.length != 0) {
            return dropdownOptions;
        } else {
            return "-";
        }
    } else {
        return "-";
    }
}