  // js/models/todo.js

  var app = app || {};

  // Todo Model
  // ----------
  // Our basic **Todo** model has `title` and `completed` attributes.

  app.It = Backbone.Model.extend({

    // Default attributes ensure that each todo created has `title` and `completed` keys.
     defaults: {
      firstitem: "Button",
      seconditem: "Checkbox",
      thirditem: "Label"
    }
});